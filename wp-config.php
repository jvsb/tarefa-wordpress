<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6zPAaRZ4nD0DtVzTDxaRD9ceuyIkIzdOcPAwkMh48mkeG3SADK8jZRRdW+QmfCtnRVBCzlzQtQXg2IeVmOJJdg==');
define('SECURE_AUTH_KEY',  'nfpn3SoSLTPnPGXnfZTG/zW3hTCO4LheoPHInJC9kms7oKn5b7/rdRzv9Pfj4TJQkeOof1wSgchohRWfJFDbrw==');
define('LOGGED_IN_KEY',    'MdSGeRDSTzQlo1aMvG5LqzBbY0wUQL/7B4BlmaiDKO/rSKrkyZr9MMfg1rffsrXiYnFwaQHBFzy5asLvoaUvGg==');
define('NONCE_KEY',        'aSfehiFhzI4eLeysBrKswmnNLxVzpD9qV1uHEu/WxlAYnw+egdUzNPT1kpBiRJQoTv2p1GgHx1E5IlEa16JEBQ==');
define('AUTH_SALT',        'FBymq+Kb1NhYn6rmGImsDwxq3PfJMKwueFlJGZlbBJcXknB87xkLpZI4ky2ySmSi+DJ5wg7k7Y6eGKGgNWZoyg==');
define('SECURE_AUTH_SALT', 'TFKX8wSESIFcVL5hMj3n7K83zIEjnY8R2kYFZDZteVavpEBF8zsUpWD5Nmm1VXSwPPuDfeCWZ1h36nr5fJ6T6A==');
define('LOGGED_IN_SALT',   'Ue2zEkoaMgm9IouYwzQh0LMH7JxsmXZmnMaNcxcl9e2LO90eV1nsEd2Qp0pmocOAeSeKhBxQ4KgJp3++K77IUQ==');
define('NONCE_SALT',       'V82fseBg171oqRJiWUoZcMDB48m8Pecru6EizkMHxpqUEQDBK6zBYUG8wpEVAINSwxraRtpgK6UobTSwJB5ExQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
