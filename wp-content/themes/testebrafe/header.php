<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Brafé</title>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css"/>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/reset.css"/>
    <?php wp_head() ?>
</head>
<body>
    <header>
       <nav>
        <a href="<?php echo get_home_url() ?>"><img id="header-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/brafe.png" alt="logo brafe"></a>
        <ul id="header-ul">
            <li><a id="header-a" href="/sobre">Sobre</a></li>
            <li><a id="header-a" href="#/produtos">Produtos</a></li>
            <li><a id="header-a" href="#/portfolio">Portfólio</a></li>
            <li><a id="header-a" href="#/contato">Contato</a></li>
        </ul>
        </nav>
    </header>