<?php
    // Template Name: Home Page
?>

<?php get_header(); ?>
<section class="container">
    <div class="imgcafe">
    <h1 class="h1">CAFÉS COM A CARA DO BRASIL</h1>
    <a class="traco">_</a>
    <a class="text1">Direto das fazendas de Minas Gerais</a>
    </div>
    <div class="posicao2" id="ancora1">
    <h2 class="h2">Uma Mistura de</h2>
    <tr>
    <td>
    <div class="div2">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cafe-1.jpg" alt="xicara de café">
        <h3 class="h3-1">Amor</h3>        
    </div>
    </td>
    <td>
    <div class="div2">
        <img class="img2" src="<?php echo get_stylesheet_directory_uri(); ?>/img/cafe-2.jpg" alt="xicara de café">
        <h3 class="h3-2">Perfeição</h3>
    </div>
    </td>
    </tr>
    <p class="psec2">O café é uma bebida produzida a partir dos grãos torrados do fruto do cafeeiro. É servido
        tradicionalmente quente, mas também pode ser consumido gelado. Ele é um estimulante,
        por possuir cafeína — geralmente 80 a 140 mg para cada 207 ml dependendo do método de
        preparação.</p>
    </div>
    </section>
    <div class="section3" id="ancora2">
            <div class="estados">
            <div class="bola1"></div>
            <h4 id="h4">Paulista</h4>
            <p class="p3">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
            tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará</p>
            </div>
            <div class="estados">
            <div class="bola2"></div>
            <h4 id="h4">Carioca</h4>
            <p class="p3">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
            tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará</p>
            </div>
            <div class="estados">
            <div class="bola3"></div>
            <h4 id="h4">Mineiro</h4>
            <p class="p3">As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
            tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará</p>
            <button class="saiba">SAIBA MAIS</button>
            </div>
    </div>
    <section class="locais">
    <div class="divs1">
    <div class="img-local">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/botafogo.jpg" alt="loja botafogo brafé">
    </div>
    <div class="texto-local">
    <h5>Botafogo</h5>
    <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
            tentou-se o cultivo.</p>
            <button>VER MAPA</button>
</div>
    </div>
    <div class="divs2">
    <div class="img-local">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/iguatemi.jpg" alt="loja iguatemi brafé">
    </div>
    <div class="texto-local">
    <h5>Iguatemi</h5>
    <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
            tentou-se o cultivo.</p>
            <button>VER MAPA</button>
</div>
    </div>
    <div class="divs3">
    <div class="img-local">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mineirao.jpg" alt="loja Mineirão brafé">
    </div>
    <div class="texto-local">
        <h5>Mineirão</h5>
        <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850,
            tentou-se o cultivo.</p>
            <button>VER MAPA</button>
    </div>
    </div>
    </section>
    <section class="section4" id="ancora3">
            <div class="assine">
                <div class="posicao3">
                <h6>Assine Nossa Newsletter</h6>
                <p>promoções e eventos mensais</p>
            </div>
                <form>
                    <label></label>
                    <input type="text" placeholder="Digite seu e-mail">
                    <button type="submit">Enviar</button>
                </form>
            </div>
    </section>
    <?php get_footer(); ?>